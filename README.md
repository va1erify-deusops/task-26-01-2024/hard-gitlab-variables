## Репозиторий, в который вынесены переменные для использования в Gitlab CI/CD в других репозиториях для решения уровня Hard [Проекта](https://gitlab.com/va1erify-deusops/task-26-01-2024/about_project)

## Переменные

- **YC_CLOUD_ID:** Идентификатор облачной платформы Yandex Cloud.
- **YC_FOLDER_ID:** Идентификатор папки в Yandex Cloud.
- **YC_CLUSTER_NAME:** Название кластера Kubernetes в Yandex Cloud.

- **CD_APP_VERSION:** Версия приложения.
- **CD_HELM_REPO_NAME:** Название репозитория Helm.
- **CD_PACKAGE_NAME:** Имя пакета в реестре.
- **CD_HELM_CHART_NAME:** Имя чарта Helm.
- **CD_USERNAME:** Имя пользователя для CD.
- **CD_PROJECT_ID:** Идентификатор репозитория с приложением.
- **CD_HELM_URL:** URL реестра Helm.
- **CD_DEPLOY_APP_IMAGE:** Имя образа для деплоя приложения.

- **CD_GITLAB_USER:** Имя пользователя GitLab.
- **CD_GITLAB_TOKEN:** Токен для доступа к GitLab.

- **CD_PATH_TO_INGRESS_CONTROLLER:** Путь к контроллеру Ingress.
- **CD_INGRESS_TAR_NAME:** Название tar-архива Ingress.
- **CD_INGRESS_MANIFEST_NAME:** Название манифеста Ingress.

- **CD_PATH_TO_CERT_MANAGER:** Путь к менеджеру сертификатов.
- **CD_CERT_MANAGER_TAR_NAME:** Название tar-архива менеджера сертификатов.
- **CD_CERT_MANAGER_MANIFEST_NAME:** Название манифеста менеджера сертификатов.

- **CD_PATH_TO_CLUSTER_ISSUER:** Путь к кластерному выпуску сертификатов.
- **CD_CLUSTER_ISSUER_TAR_NAME:** Название tar-архива кластерного выпуска сертификатов.
- **CD_CLUSTER_ISSUER_MANIFEST_NAME:** Название манифеста кластерного выпуска сертификатов.

- **INFRASTRUCTURE_TF_ROOT:** Относительный путь к корневой директории проекта Terraform.
- **INFRASTRUCTURE_TF_STATE_NAME:** Имя файла состояния Terraform.